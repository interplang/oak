#![allow(non_snake_case)]
#![allow(unused_variables)]

use interplang::vm::value::Value;
use interplang::parser::loc::Loc;

#[no_mangle]
pub fn Interp_Oak_WinFix(args: Vec<(Value, Loc)>, named_args: Vec<(String, Value, Loc)>, loc: Loc) -> Value {
    print!("");
    Value::Void
}

#[no_mangle]
pub fn Interp_Oak_Sin(args: Vec<(Value, Loc)>, named_args: Vec<(String, Value, Loc)>, loc: Loc) -> Value {
    let Value::Float(n) = args[0].0.clone() else {
        return Value::Null;
    };
    Value::Float(n.sin())
}

#[no_mangle]
pub fn Interp_Oak_Cos(args: Vec<(Value, Loc)>, named_args: Vec<(String, Value, Loc)>, loc: Loc) -> Value {
    let Value::Float(n) = args[0].0.clone() else {
        return Value::Null;
    };
    Value::Float(n.cos())
}

#[no_mangle]
pub fn Interp_Oak_Tan(args: Vec<(Value, Loc)>, named_args: Vec<(String, Value, Loc)>, loc: Loc) -> Value {
    let Value::Float(n) = args[0].0.clone() else {
        return Value::Null;
    };
    Value::Float(n.tan())
}

#[no_mangle]
pub fn Interp_Oak_Asin(args: Vec<(Value, Loc)>, named_args: Vec<(String, Value, Loc)>, loc: Loc) -> Value {
    let Value::Float(n) = args[0].0.clone() else {
        return Value::Null;
    };
    Value::Float(n.asin())
}

#[no_mangle]
pub fn Interp_Oak_Acos(args: Vec<(Value, Loc)>, named_args: Vec<(String, Value, Loc)>, loc: Loc) -> Value {
    let Value::Float(n) = args[0].0.clone() else {
        return Value::Null;
    };
    Value::Float(n.acos())
}

#[no_mangle]
pub fn Interp_Oak_Atan(args: Vec<(Value, Loc)>, named_args: Vec<(String, Value, Loc)>, loc: Loc) -> Value {
    let Value::Float(n) = args[0].0.clone() else {
        return Value::Null;
    };
    Value::Float(n.atan())
}

#[no_mangle]
pub fn Interp_Oak_Sinh(args: Vec<(Value, Loc)>, named_args: Vec<(String, Value, Loc)>, loc: Loc) -> Value {
    let Value::Float(n) = args[0].0.clone() else {
        return Value::Null;
    };
    Value::Float(n.sinh())
}

#[no_mangle]
pub fn Interp_Oak_Cosh(args: Vec<(Value, Loc)>, named_args: Vec<(String, Value, Loc)>, loc: Loc) -> Value {
    let Value::Float(n) = args[0].0.clone() else {
        return Value::Null;
    };
    Value::Float(n.cosh())
}

#[no_mangle]
pub fn Interp_Oak_Atan2(args: Vec<(Value, Loc)>, named_args: Vec<(String, Value, Loc)>, loc: Loc) -> Value {
    let Value::Float(n) = args[0].0.clone() else {
        return Value::Null;
    };
    let Value::Float(n2) = args[1].0.clone() else {
        return Value::Null;
    };
    Value::Float(n.atan2(n2))
}

#[no_mangle]
pub fn Interp_Oak_SleepMS(args: Vec<(Value, Loc)>, named_args: Vec<(String, Value, Loc)>, loc: Loc) -> Value {
    let Value::Int(n) = args[0].0.clone() else {
        return Value::Null;
    };
    std::thread::sleep(std::time::Duration::from_millis(n as u64));
    Value::Void
}

#[no_mangle]
pub fn Interp_Oak_Quit(args: Vec<(Value, Loc)>, named_args: Vec<(String, Value, Loc)>, loc: Loc) -> Value {
    let Value::Int(n) = args[0].0.clone() else {
        return Value::Null;
    };
    std::process::exit(n as i32);
}

#[no_mangle]
pub fn Interp_Oak_TimeNow(args: Vec<(Value, Loc)>, named_args: Vec<(String, Value, Loc)>, loc: Loc) -> Value {
    Value::Int(std::time::SystemTime::now().duration_since(std::time::UNIX_EPOCH).unwrap().as_millis() as i64)
}

#[no_mangle]
pub fn Interp_Oak_Elapsed(args: Vec<(Value, Loc)>, named_args: Vec<(String, Value, Loc)>, loc: Loc) -> Value {
    let Value::Int(n) = args[0].0.clone() else {
        return Value::Null;
    };
    let duration = std::time::Duration::from_millis(n as u64);
    let time = std::time::UNIX_EPOCH + duration;
    Value::Int(std::time::SystemTime::now().duration_since(time).unwrap().as_millis() as i64)
}

#[no_mangle]
pub fn Interp_Oak_Init(args: Vec<(Value, Loc)>, named_args: Vec<(String, Value, Loc)>, loc: Loc) -> Value {
    let Value::Int(n) = args[0].0.clone() else {
        return Value::Null;
    };
    let mut vec = Vec::with_capacity(n as usize);
    for _ in 0..n {
        vec.push(args[1].0.clone());
    }
    Value::List(vec)
}

#[no_mangle]
pub fn Interp_Oak_CharToCodePoint(args: Vec<(Value, Loc)>, named_args: Vec<(String, Value, Loc)>, loc: Loc) -> Value {
    let Value::String(str) = args[0].0.clone() else {
        return Value::Null;
    };
    Value::Int(str.chars().next().unwrap() as i64)
}


#[no_mangle]
pub fn Interp_Oak_CodePointToChar(args: Vec<(Value, Loc)>, named_args: Vec<(String, Value, Loc)>, loc: Loc) -> Value {
    let Value::Int(n) = args[0].0.clone() else {
        return Value::Null;
    };
    match char::from_u32(n as u32) {
        Some(a) => Value::String(a.to_string()),
        None => Value::Null
    }
}

#[no_mangle]
pub fn Interp_Oak_ToLower(args: Vec<(Value, Loc)>, named_args: Vec<(String, Value, Loc)>, loc: Loc) -> Value {
    let Value::String(str) = args[0].0.clone() else {
        return Value::Null;
    };
    Value::String(str.to_lowercase())
}

#[no_mangle]
pub fn Interp_Oak_ToUpper(args: Vec<(Value, Loc)>, named_args: Vec<(String, Value, Loc)>, loc: Loc) -> Value {
    let Value::String(str) = args[0].0.clone() else {
        return Value::Null;
    };
    Value::String(str.to_uppercase())
}

#[no_mangle]
pub fn Interp_Oak_SetWorkingDir(args: Vec<(Value, Loc)>, named_args: Vec<(String, Value, Loc)>, loc: Loc) -> Value {
    let Value::String(str) = args[0].0.clone() else {
        return Value::Null;
    };
    match std::env::set_current_dir(std::path::Path::new(&str)) {
        Ok(()) => Value::Boolean(true),
        Err(_) => Value::Boolean(false)
    }
}

#[no_mangle]
pub fn Interp_Oak_Glob(args: Vec<(Value, Loc)>, named_args: Vec<(String, Value, Loc)>, loc: Loc) -> Value {
    let Value::String(str) = args[0].0.clone() else {
        return Value::Null;
    };
    let mut out_paths = vec![];
    match glob::glob(&str) {
        Ok(paths) => {
            for path in paths {
                match path {
                    Ok(path) => out_paths.push(Value::String(path.to_string_lossy().to_string())),
                    Err(_) => {}
                }
            }
        }
        Err(_) => return Value::Null
    }
    Value::List(out_paths)
}

#[no_mangle]
pub fn Interp_Oak_IsFile(args: Vec<(Value, Loc)>, named_args: Vec<(String, Value, Loc)>, loc: Loc) -> Value {
    let Value::String(str) = args[0].0.clone() else {
        return Value::Null;
    };
    match std::fs::metadata(&str) {
        Ok(md) => {
            if md.is_file() {
                return Value::String("file".to_owned());
            }
            if md.is_dir() {
                return Value::String("dir".to_owned());
            }
        }
        Err(_) => {}
    }
    Value::Null
}

#[no_mangle]
pub fn Interp_Oak_RmFile(args: Vec<(Value, Loc)>, named_args: Vec<(String, Value, Loc)>, loc: Loc) -> Value {
    let Value::String(str) = args[0].0.clone() else {
        return Value::Null;
    };
    let Value::String(mode) = args[1].0.clone() else {
        return Value::Null;
    };
    match mode.as_str() {
        "file" => {
            let _ = std::fs::remove_file(&str);
        }
        "dir" => {
            let _ = std::fs::remove_dir_all(&str);
        }
        _ => {}
    }
    Value::Void
}
