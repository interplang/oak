#################################################
# This file is a part of the Oak Interp library #
#################################################

imp "iter.it";

use iter;

mod string {

    `<doc>
    Returns true if the string starts with the pattern.
    </doc>`
    fun StartsWith(string String, pattern String, startingIndex Int[0..] = 0) Bool {
        var string_len = Len(string);
        var pattern_len = Len(pattern);
        if string_len - startingIndex < pattern_len {
            ret false;
        }
        var i = 0;
        while i < pattern_len {
            if string[startingIndex + i] != pattern[i] {
                ret false;
            }
            i += 1;
        }
        ret true;
    }

    `<doc>
    Returns true if the string ends with the pattern.
    </doc>`
    fun EndsWith(string String, pattern String) Bool {
        if pattern == "" {
            ret true;
        }
        var string_len = Len(string);
        var pattern_len = Len(pattern);
        if string_len < pattern_len {
            ret false;
        }
        var sliced = string[-pattern_len..];
        ret sliced == pattern;
    }

    `<doc>
    Returns the index of the patterns first character where the pattern is found in the string, or null.
    </doc>`
    fun Find(string String, pattern String) Int or Null {
        var i = 0;
        while string != "" {
            if StartsWith(string, pattern) {
                ret i;
            }
            string = string[1..];
            i += 1;
        }
        ret null;
    }

    `<doc>
    Returns true if the pattern is found as a substring inside the string.
    </doc>`
    fun Contains(string String, pattern String) Bool {
        ret Find(string, pattern) != null;
    }

    var WHITESPACE = [" ", "\t", "\n", "\t"];

    `<doc>
    Remove every occurance of the pattern(s) from the start of the string.
    </doc>`
    fun TrimStart(string String, pattern String or List<String> = WHITESPACE) String {
        if pattern is String {
            var pattern_len = Len(pattern);
            while StartsWith(string, pattern) {
                string = string[pattern_len..];
            }
            ret string;
        }
        var temp_string = string;
        do p in ForEach(pattern) {
            string = TrimStart(string, p);
        }
        if temp_string == string {
            ret string;
        }
        ret TrimStart(string, pattern);
    }

    `<doc>
    Remove every occurance of the pattern(s) from the end of the string.
    </doc>`
    fun TrimEnd(string String, pattern String or List<String> = WHITESPACE) String {
        if pattern is String {
            var pattern_len = Len(pattern);
            while EndsWith(string, pattern) {
                string = string[..-pattern_len];
            }
            ret string;
        }
        var temp_string = string;
        do p in ForEach(pattern) {
            string = TrimEnd(string, p);
        }
        if temp_string == string {
            ret string;
        }
        ret TrimEnd(string, pattern);
    }

    `<doc>
    Remove every occurance of the pattern(s) from the string.
    </doc>`
    fun Trim(string String, pattern String or List<String> = WHITESPACE) String {
        string = TrimStart(string, pattern);
        string = TrimEnd  (string, pattern);
        ret string;
    }

    `<doc>
    Splits the string into substrings.
    </doc>`
    fun Split(string String, separator String = "", ignoreEmpties Bool = false) List<String> {
        var list = [];
        var sep_len = Len(separator);
        if sep_len == 0 {
            var i = 0;
            while string[i] != void {
                list += [string[i]];
                i += 1;
            }
            ret list;
        }
        var str_len = Len(string);
        var i = 0;
        var start_i = 0;
        while i < str_len {
            if StartsWith(string, separator, i) {
                if start_i != i + 1 {
                    if start_i == i {
                        if !ignoreEmpties {
                            list.push("");
                        }
                    } else {
                    list.push(string[start_i..i]);
                    }
                }
                i += sep_len;
                start_i = i;
            } else {
                i += 1;
            }
        }
        list.push(string[start_i..]);
        ret list;
    }

    `<doc>
    Replace every occurance of "from" to "to".
    </doc>`
    fun Replace(string String, from String, to String) String {
        if from == "" {
            ret string;
        }
        var out_string = "";
        var from_len = Len(from);
        while string != "" {
            if StartsWith(string, from) {
                out_string += to;
                string = string[from_len..];
            } else {
                out_string += string[0];
                string = string[1..];
            }
        }
        ret out_string;
    }

    `<doc>
    Join strings together separarted by another string.
    </doc>`
    fun StringJoin(strings List<String>, separator String = "") String {
        if Len(strings) == 0 {
            ret "";
        }
        var out = strings[0];
        var i = 1;
        while i < Len(strings) {
            out += separator;
            out += strings[i];
            i += 1;
        }
        ret out;
    }

    `<doc>
    Returns the unicode code point for the specified character.
    char must be a single character.
    </doc>`
    fun CharToUnicode(char String) Int or Null {
        if Len(char) != 1 {
            ret null;
        }
        ret os::private_foreign_functions.char_to_unicode(char);
    }

    `<doc>
    Returns the character for the specified unicode code point.
    char must be a single character.
    </doc>`
    fun UnicodeToChar(codepoint Int) String or Null {
        ret os::private_foreign_functions.unicode_to_char(codepoint);
    }

    `<doc>
    Returns the lowercase variant of the string.
    </doc>`
    fun Lowercase(string String) String {
        ret os::private_foreign_functions.lowercase(string);
    }

    `<doc>
    Returns the uppercase variant of the string.
    </doc>`
    fun Uppercase(string String) String {
        ret os::private_foreign_functions.uppercase(string);
    }

}
