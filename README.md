# oak
Oak is the de facto standard library for the Interp scripting language.

## Packi Package
Packi package 'oak' available [here](https://packi.kriikkula.net/oak).

## Documentation
Documentation available [here](https://interp.kriikkula.net/oak.html)
